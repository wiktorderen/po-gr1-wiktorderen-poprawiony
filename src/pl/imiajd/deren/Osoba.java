package pl.imiajd.deren;

public class Osoba {
    private String nazwisko;
    private int rokUrodzenia;

    public Osoba(){}

    public Osoba(String nazwisko, int rokUrodzenia){
        this.nazwisko=nazwisko;
        this.rokUrodzenia=rokUrodzenia;
    }

    public String getNazwisko(){
        return nazwisko;
    }

    public int getRokUrodzenia(){
        return rokUrodzenia;
    }
}
