package pl.imiajd.deren;

import pl.imiajd.deren.Osoba;

public class Nauczyciel extends Osoba {
    private int pensja;

    public Nauczyciel(String nazwisko, int rokUrodzenina, int pensja){
        super(nazwisko, rokUrodzenina);
        this.pensja=pensja;
    }

    public String toString(){
        return getNazwisko() +" "+getRokUrodzenia()+" "+getPensja();
    }

    public String getNazwisko(){
        return super.getNazwisko();
    }

    public int getRokUrodzenia(){
        return super.getRokUrodzenia();
    }

    public int getPensja(){
        return pensja;
    }
}
