package pl.imiajd.deren;

import pl.imiajd.deren.Osoba;

public class Student extends Osoba {
    private String kierunek;

    public Student(String nazwisko, int rokUrodzenia, String kierunek){
        super(nazwisko,rokUrodzenia);
        this.kierunek=kierunek;
    }

    public String toString(){
        return getNazwisko()+" "+getRokUrodzenia()+" "+getKierunek();
    }

    public String getNazwisko(){
        return super.getNazwisko();
    }

    public int getRokUrodzenia(){
        return super.getRokUrodzenia();
    }

    public String getKierunek(){
        return kierunek;
    }
}
