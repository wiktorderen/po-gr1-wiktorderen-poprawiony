package pl.imiajd.deren;
import java.lang.String;

public class Adres {
   private String ulica,numer_domu,numer_mieszkania,miasto,kod_pocztowy;

    public Adres(String ul, String nd, String nm, String m, String k ) {
        ulica=ul;
        numer_domu=nd;
        numer_mieszkania=nm;
        miasto=m;
        kod_pocztowy=k;
    }

    public Adres(String ul, String nd, String m, String k) {
        ulica=ul;
        numer_domu=nd;
        miasto=m;
        kod_pocztowy=k;
    }

    public void pokaz(){
        System.out.println(kod_pocztowy +" "+ miasto);
        System.out.println(ulica +" "+ numer_domu+"/"+numer_mieszkania);
    }

    public boolean przed(Adres a, Adres b){
        int wynik = a.kod_pocztowy.compareTo(b.kod_pocztowy);
        if(wynik==0) {
            return false;
        }
        return true;
    }
}
