package pl.edu.uwm.wmii.wiktorderen.lista08;
import java.sql.SQLOutput;
import java.util.*;

public class TestOsoba_zmodyfikowany {
    public void main (String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0]= new Pracownik("Kowalski", 50000);
        ludzie[1]=new Student(" Nowak", "informatyka");

        for(Osoba p: ludzie)
        {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }
    }

    abstract class Osoba
    {
        private String nazwisko;
        public Osoba(String nazwisko)
        {
            this.nazwisko=nazwisko;
        }
        public abstract String getOpis();
        public String getNazwisko()
        {
            return nazwisko;
        }
    }

    class Pracownik extends Osoba{
        private double pobory;
        public Pracownik(String nazwisko, double pobory)
        {
            super(nazwisko);
            this.pobory=pobory;
        }
        public double getPobory()
        {
            return pobory;
        }
        public String getOpis()
        {
            return String.format("pracownik z pensją %.2f zł", pobory);
        }
    }
    class Student extends Osoba{
        private String kierunek;
        public Student(String nazwisko, String kierunek)
        {
            super(nazwisko);
            this.kierunek=kierunek;
        }
        public String getOpis()
        {
            return "kierunek studiów: " + kierunek;
        }
    }
}
