package pl.edu.uwm.wmii.wiktorderen.lista08;

import java.time.LocalDate;

public class Osoba {
    private String imona;
    private String nazwisko;
    private int rokUrodzenia;
    private LocalDate dataUrodzenia;
    private boolean plec;


    public Osoba() {
    }

    public Osoba(String imiona, String nazwisko, int rokUrodzenia, LocalDate dataUrodzenia, boolean plec) {
        this.imona = imiona;
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    public String getImona() {
        return imona;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getRokUrodzenia() {
        return rokUrodzenia;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public boolean getplec(){return plec;}
}
