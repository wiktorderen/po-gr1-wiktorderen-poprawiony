package pl.edu.uwm.wmii.wiktorderen.lista02;

import java.util.Random;
import java.util.Scanner;

public class Zad1b {
    public static void main(String[] args) {
    int n;
    Scanner sc = new Scanner(System.in);
    System.out.print("Podaj liczbe: ");
    n = sc.nextInt();
    Random r = new Random();

    int[] tablica = new int[n];
    for (int i = 0; i < tablica.length; i++)
    {
        tablica[i] = r.nextInt() % 1000;
    }
    int ujemne=0;
    int dodatnie=0;
    int zera=0;
    for (int i = 0; i < tablica.length; i++)
    {

        System.out.println(tablica[i]);
        if(tablica[i]<0)
        {
            ujemne++;
        }
        if(tablica[i]>0)
        {
            dodatnie++;
        }
        if(tablica[i]==0)
        {
            zera++;
        }
    }
    System.out.println("Ilosc liczb ujemnych: " +ujemne);
    System.out.println("Ilosc liczb dodatnich: " +dodatnie);
    System.out.println("Ilosc zer: " +zera);
}
}
