package pl.edu.uwm.wmii.wiktorderen.lista02;

import java.util.Random;

public class Generuj {
        public static void wpisz ( int tab[], int n, int minWartosc, int maxWartosc){
            int a[] = new int[n];
            losuj(tab,maxWartosc);
            wypisz(tab);
        }

        public static void losuj ( int[] tab, int max){
            Random r = new Random();
            for (int j = 0; j < tab.length; ++j) {
                tab[j] = r.nextInt(max);
            }
        }

        public static void wypisz ( int[] tab){
            for (int el : tab) {
                System.out.print(el + " ");
            }
            System.out.println("");
        }
    }
