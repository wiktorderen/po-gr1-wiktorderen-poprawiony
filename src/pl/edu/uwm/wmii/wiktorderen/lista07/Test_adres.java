package pl.edu.uwm.wmii.wiktorderen.lista07;

import pl.imiajd.deren.Adres;

public class Test_adres {
    public static void main(String[] args) {
        Adres adres1 = new Adres("Słoneczna", "24", "12","Olsztyn", "10-422");
        Adres adres2 = new Adres("Promienista", "15", "56", "Olsztyn", "06-355");
        adres1.pokaz();
        adres2.pokaz();
        adres1.przed(adres1,adres2);
    }
}
