package pl.edu.uwm.wmii.wiktorderen.lista07;

import pl.imiajd.deren.Nauczyciel;
import pl.imiajd.deren.Student;

public class Test_Osoba {
    public static void main(String[] args) {
        Student student = new Student("Dereń", 1997, "Informatyka");
        Nauczyciel nauczyciel = new Nauczyciel("Wiśniewski", 1962, 7000);
        student.toString();
        nauczyciel.toString();
    }
}
