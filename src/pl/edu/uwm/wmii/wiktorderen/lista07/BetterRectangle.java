package pl.edu.uwm.wmii.wiktorderen.lista07;

import java.awt.*;

public class BetterRectangle extends Rectangle {
    private int permieter;
    private float area;

    public BetterRectangle(int obwod, float powierzchnia){
        permieter=obwod;
        area=powierzchnia;
    }

    public int getPermieter() {return permieter;}

    public float getArea() {return area;}
}
