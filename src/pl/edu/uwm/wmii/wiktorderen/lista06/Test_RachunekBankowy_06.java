package pl.edu.uwm.wmii.wiktorderen.lista06;
public class Test_RachunekBankowy_06 {
   public static void main(String[] args) {
      RachunekBankowy_06 saver1 = new RachunekBankowy_06(2000);
       RachunekBankowy_06 saver2  = new RachunekBankowy_06(3000);
       RachunekBankowy_06.setRocznaStopaProcentowa(0.04);
       saver1.obliczMiesieczneOdsetki();
       saver2.obliczMiesieczneOdsetki();
       System.out.println("saver1 "+saver1.getSaldo());
       System.out.println("saver2 "+saver2.getSaldo());
       System.out.println();
       RachunekBankowy_06.setRocznaStopaProcentowa(0.05);
       saver1.obliczMiesieczneOdsetki();
       saver2.obliczMiesieczneOdsetki();
       System.out.println("saver1 "+saver1.getSaldo());
       System.out.println("saver2 "+saver2.getSaldo());

   }
}
