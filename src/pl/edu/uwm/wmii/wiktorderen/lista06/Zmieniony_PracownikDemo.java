package pl.edu.uwm.wmii.wiktorderen.lista06;

public class Zmieniony_PracownikDemo {
    public static void main(String[] args)
    {
        Zmieniony_Pracownik[] personel = new Zmieniony_Pracownik[3];

        // wypełnij tablicę danymi pracowników
        personel[0] = new Zmieniony_Pracownik("Karol Cracker", 75000, 2001, 12, 15);
        personel[1] = new Zmieniony_Pracownik("Henryk Hacker", 50000, 2003, 10, 1);
        personel[2] = new Zmieniony_Pracownik("Antoni Tester", 40000, 2005, 3, 15);

        // zwieksz pobory każdego pracownika o 20%
        for (Zmieniony_Pracownik e : personel) {
            e.zwiekszPobory(20);
        }

        // wypisz informacje o każdym pracowniku
        for (Zmieniony_Pracownik e : personel) {
            System.out.print("nazwisko = " + e.nazwisko() + "\tid = " + e.id());
            System.out.print("\tpobory = " + e.pobory());
            System.out.printf("\tdataZatrudnienia = %tF\n", e.dataZatrudnienia());
        }
        System.out.println();

        int n = Zmieniony_Pracownik.getNextId(); // wywołanie metody statycznej
        System.out.println("Następny dostępny id = " + n);

    }
}