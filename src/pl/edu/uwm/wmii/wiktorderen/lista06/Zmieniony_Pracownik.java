package pl.edu.uwm.wmii.wiktorderen.lista06;
import javax.xml.crypto.Data;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.GregorianCalendar;

public class Zmieniony_Pracownik {
    public Zmieniony_Pracownik(String nazwisko, double pobory, int year, int month, int day)
    {
        this.nazwisko = nazwisko;
        this.pobory = pobory;

        GregorianCalendar calendar = new GregorianCalendar(year, month - 1, day);
        dataZatrudnienia = LocalDate.now();

        id = nextId;
        ++nextId;
    }

    public String nazwisko()
    {
        return nazwisko;
    }

    public double pobory()
    {
        return pobory;
    }

    public LocalDate dataZatrudnienia()
    {
        return (LocalDate) dataZatrudnienia;
    }

    public void zwiekszPobory(double procent)
    {
        double podwyżka = pobory * procent / 100;
        pobory += podwyżka;
    }

    public int id()
    {
        return id;
    }

    public void setId()
    {
        id = nextId;
        ++nextId;
    }

    public static int getNextId()
    {
        return nextId;
    }

    private String nazwisko;
    private double pobory;
    private LocalDate dataZatrudnienia;

    private int id;
    private static int nextId = 1;
}
