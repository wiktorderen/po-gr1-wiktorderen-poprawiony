package pl.edu.uwm.wmii.wiktorderen.lista01;

import java.util.Scanner;

public class Zad_II_3 {
    public static void main(String[] args) {
        int n = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Podaj liczbe elemntow: ");
        n = sc.nextInt();
        int i = 0;
        int dodatnie=0;
        int ujemne=0;
        int zera=0;
        while (i < n) {
            System.out.println("Podaj element" + (i + 1) + ": ");
            int x = sc.nextInt();
            if(x>0) dodatnie++;
            if(x<0) ujemne++;
            if(x==0) zera++;
            i++;
        }
        System.out.println("Ilosc liczb dodatnich: " +dodatnie);
        System.out.println("Ilosc liczb ujemnych: " +ujemne);
        System.out.println("Ilosc zer: " +zera);


    }
}
